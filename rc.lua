-- Declare globals
Awful = require("awful")
Gears = require("gears")
Wibox = require("wibox")

-- load luarocks if installed
pcall(require, "luarocks.loader")

-- load theme
Beautiful = require("beautiful")
Dpi = Beautiful.xresources.apply_dpi
Beautiful.init("~/.config/awesome/theme/futurepixels.lua")

-- load modules
Focus = require("modules.focus")

-- load plugins
Revelation = require("plugins.revelation")

-- load key and mouse bindings
require("bindings")

-- load rules
require("rules")

-- load signals
require("signals")

Revelation.init()

PowerMenu = require("widgets.powermenu")
Dashboard = require("widgets.dashboard")

os.execute("~/.screenlayout/mother.sh &")
os.execute("~/.config/awesome/support/scripts/autostart.sh &")
