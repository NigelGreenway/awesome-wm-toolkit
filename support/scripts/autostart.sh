#!/bin/sh

###
# A script to help manage auto start of applets
# using `xrdb` and `dex`
###

#AUTOSTART_ITEMS=$(dex --environment Awesome --dry-run --autostart | cut -d ':' -f2 | cut -d ' ' -f2 | rev | cut -d '/' -f1 | rev)
#DEBUG=true
##TODO: stop this happening:
#IGNORED_ITEMS=(sh) # This stops sh being killed as kills terminalTODO
#DEFAULT_NAMESPACE_PREFIX="awesome.autostart"


if (xrdb -query | grep -q "^awesome\\.started:\\s*true$"); then exit; fi;
xrdb -merge <<< "awesome.started:true"

dex --environment Awesome --autostart
clipit



####
## Record all items about to be ran via dex into `xrdb`
####
#start_applets() {
#  __debug "Autostart running"

#  xrdb -merge <<< "awesome.autostart:true"
#  __updateDBCommand "" "true" "awesome.autostart."

#  for cmd in ${AUTOSTART_ITEMS[@]}; do
#    local is_ignored=$(__arrayContainsItem "$cmd")

#    if [ "$is_ignored" == 1 ]; then
#      __debug "$cmd is ignored"

#      continue
#    fi

#    __debug "Starting $cmd"

#    local is_displayed_in_system_tray=$(__queryCommand ${cmd})

#    if [ "$is_displayed_in_system_tray" == true ]; then
#      pkill -9 ${cmd:0:15}
#    fi

#    __updateDBCommand $cmd "true"
#  done

#  __debug "run dex --environment awesome --autostart"
#  dex --environment Awesome --autostart
#}

#stop() {
#  for cmd in ${AUTOSTART_ITEMS[@]}; do
#    local is_ignored=$(__arrayContainsItem "$cmd")

#    if [ "$is_ignored" == 1 ]; then
#      __debug "$cmd is ignored"

#      continue
#    fi

#    __debug "Loading $cmd from xrdb"
#    local is_displayed_in_system_tray=$(__queryCommand ${cmd})

#    __debug "$is_displayed_in_system_tray"
#    if [ "$is_displayed_in_system_tray" == true ]; then

#      pkill -9 ${cmd:0:15}
#      __updateDBCommand ${cmd} "false"
#    fi
#  done
#}

#__debug() {
#  [ "$DEBUG" == true ] && echo "DEBUG: $1"
#}

#__arrayContainsItem() {
#  result=$(printf '%s\0' "${IGNORED_ITEMS[@]}" | grep -F -x -z -- "$1" | xargs --null echo)

#  if [ "${#result}" -gt 0 ]; then
#    echo 1
#    return
#  fi

#  echo 0
#  return
#}

#__queryCommand() {
#  application=$1
#  namespace_prefix=${2:-"awesome.autostart."}

#  xrdb -get "${namespace_prefix}.${application}"
#}

#__updateDBCommand() {
#  application=$1
#  value=$2
#  namespace_prefix=${3:-"awesome.autostart."}

#  xrdb -merge <<< "${namespace_prefix}.${application}:${value}"
#}

#if [ "$1" == "start" ]; then
#  __debug "Starting applets"
#  start_applets
#fi


#if [ "$1" == "focus" ]; then
#  __debug "Starting focus"
#  stop
#fi
