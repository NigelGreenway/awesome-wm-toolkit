# README

A toolkit to help people start with a baseline for a theme and ease into customisation.

## Plugins used

 - https://github.com/guotsuan/awesome-revelation | Expose like feature as found in macOS  

## Dependencies

 - maim  
 - betterlockscreen  
