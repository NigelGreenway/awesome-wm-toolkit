local awful = require("awful")
local wibox = require("wibox")
local gears = require("gears")
local vars = require("config.vars")
local beautiful = require("beautiful")
local dpi = beautiful.xresources.apply_dpi

-- local dashboard = awful.popup {
--   screen         = screen[2],
--   widget         = wibox.container.background,
--   ontop          = true,
--   bg             = beautiful.bg_normal,
--   visible        = true,
--   placement      = function(c)
--     awful.placement.centered_top(c,
--     { margins    = { top = 0, bottom = 8, left = 8, right = 8 } })
--   end,
--   shape          = function(cr, width, height)
--     gears.shape.rounded_rect(cr, width, height, 0)
--   end,
--   opacity        = 1,
--   forced_width   = vars.screen.width,
--   forced_height  = vars.screen.height,
-- }

local home = wibox.widget({
	{
		widget = wibox.widget.textbox("Welcome to your dashboard"),
	},
	layout = wibox.layout.fixed.vertical,
})

-- dashboard:setup {
--   {
--     home,
--     layout = wibox.layout.stack,
--   },
--   widget = wibox.container.background,
--   bg     = beautiful.bg_normal,
-- }

-- dashboard:setup {
--   {
--     {
--       home,
--       -- Separator,
--       layout = wibox.layout.stack
--     },
--     -- sidebar,
--     layout = wibox.layout.fixed.horizontal
--   },
--   widget = wibox.container.background,
--   bg = beautiful.bg_normal,
--   -- shape = function(cr, width, height)
--   --   gears.shape.rounded_rect(cr, width, height, 10)
--   -- end,
-- }
-- awful.popup {
--   screen          = screen[2],
--   placement      = function(c)
--     awful.placement.stretch(c,
--     { margins    = { top = dpi(40), bottom = dpi(0), left = dpi(0), right = dpi(0) } })
--   end,
--   forced_width   = vars.screen.width,
--   forced_height  = dpi(900),
--   bg = beautiful.bg_normal,
--   border_width = 5,
--   widget = {
--     forced_width   = vars.screen.width / 2,
--     forced_height  = dpi(400),
--     screen         = screen[2],
--     widget         = wibox.container.background,
--     ontop          = true,
--     bg             = beautiful.bg_normal,
--     visible        = true,
--     shape          = function(cr, width, height)
--       gears.shape.rounded_rect(cr, width, height, 0)
--     end,
--     opacity        = 1,
--   }
-- }

local dashboard = awful.popup({
	widget = wibox.widget({
		text = "Dashboard",
		forced_height = dpi(600),
		forced_width = dpi(1200),
		widget = home,
	}),
	ontop = true,
	screen = screen[1],
	visible = false,
	-- placement = awful.placement.center_horizontal,
	y = dpi(140),
	placement = function(c)
		awful.placement.center_horizontal(
			c,
			{ margins = { top = dpi(40), bottom = dpi(0), left = dpi(0), right = dpi(0) } }
		)
	end,
})

return dashboard
