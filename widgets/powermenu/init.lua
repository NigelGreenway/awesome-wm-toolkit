local awful = require("awful")
local wibox = require("wibox")
local gears = require("gears")
local beautiful = require("beautiful")
local vars      = require("config.vars")
local dpi = beautiful.xresources.apply_dpi

local color = require("widgets.powermenu.color")
local text = require("widgets.powermenu.text")

--Buttons
local buttons = require("widgets.powermenu.buttons")

--Separator widget
local Separator = wibox.widget.textbox("")
Separator.forced_height = vars.screen.height
Separator.forced_width = vars.screen.width

local Separator2 = wibox.widget.textbox("")
Separator2.forced_width = dpi(90)

--Main powermenu wibox
local powermenu = awful.popup {
  screen = screen[1],
  widget = wibox.container.background,
  ontop = true,
  bg = beautiful.bg_normal,
  visible = false,
  placement = function(c)
    awful.placement.centered(c,
    { margins = { top = dpi(0), bottom = dpi(0), left = dpi(0), right = dpi(0) } })
  end,
  shape = function(cr, width, height)
    gears.shape.rounded_rect(cr, width, height, 0)
  end,
  opacity = 1,
  forced_height = vars.screen.height,
  forced_width = vars.screen.width,
}

powermenu:setup {
  {
    Separator,
    {
      {
        {
          buttons.shutdown,
          Separator2,
          buttons.reboot,
          Separator2,
          buttons.logout,
          Separator2,
          buttons.sleep,
          Separator2,
          buttons.lock,
          layout = wibox.layout.fixed.horizontal
        },
        layout = wibox.layout.fixed.vertical
      },
      layout = wibox.container.place
    },
    layout = wibox.layout.stack
  },
  widget = wibox.container.background,
  bg = beautiful.bg_normal
}

powermenu:connect_signal("button::press", function(_, _, _, button)
  if button == 3 then
    awesome.emit_signal("widget::control")
    powermenu.visible = false
  end
end)

awesome.connect_signal("widget::powermenu", function()
  powermenu.visible = false
end)

awesome.connect_signal("widget::powermenu2", function()
  powermenu.visible = true
end)

return powermenu
