local _M = {}

local awful = require("awful")
local hotkeys_popup = require("awful.hotkeys_popup")
local beautiful = require("beautiful")
local dpi = beautiful.xresources.apply_dpi
local wibox = require("wibox")
local gears = require("gears")

local focus_mode_widget = require("modules.focus.system_bar_widget")

local apps = require("config.apps")
local mod = require("bindings.mod")
local vars = require("config.vars")

_M.awesomemenu = {
	{
		"hotkeys",
		function()
			hotkeys_popup.show_help(nil, awful.screen.focused())
		end,
	},
	{ "manual", apps.manual_cmd },
	{ "edit config", apps.editor_cmd .. " " .. awesome.conffile },
	{ "restart", awesome.restart },
	{
		"quit",
		function()
			awesome.quit()
		end,
	},
}

-- _M.mainmenu = awful.menu({
-- 	items = {
-- 		{ "awesome", _M.awesomemenu, beautiful.awesome_icon },
-- 		{ "open terminal", apps.terminal },
-- 	},
-- })

-- _M.launcher = awful.widget.launcher({
-- 	image = beautiful.awesome_icon,
-- 	menu = _M.mainmenu,
-- })

_M.keyboardlayout = awful.widget.keyboardlayout()
_M.textclock = wibox.widget.textclock()

function _M.create_promptbox()
	return awful.widget.prompt()
end

function _M.create_layoutbox(s)
	return awful.widget.layoutbox({
		screen = s,
		buttons = {
			awful.button({
				modifiers = {},
				button = 1,
				on_press = function()
					awful.layout.inc(1)
				end,
			}),
			awful.button({
				button = 3,
				on_press = function()
					awful.layout.inc(-1)
				end,
			}),
			awful.button({
				modifiers = {},
				button = 4,
				on_press = function()
					awful.layout.inc(-1)
				end,
			}),
			awful.button({
				modifiers = {},
				button = 5,
				on_press = function()
					awful.layout.inc(1)
				end,
			}),
		},
	})
end

function _M.create_taglist(s)
	return awful.widget.taglist({
		screen = s,
		filter = awful.widget.taglist.filter.all,
		buttons = {
			awful.button({
				modifiers = {},
				button = 1,
				on_press = function(t)
					t:view_only()
				end,
			}),
			awful.button({
				modifiers = { mod.super },
				button = 1,
				on_press = function(t)
					if client.focus then
						client.focus:move_to_tag(t)
					end
				end,
			}),
			awful.button({
				modifiers = {},
				button = 3,
				on_press = awful.tag.viewtoggle,
			}),
			awful.button({
				modifiers = { mod.super },
				button = 3,
				on_press = function(t)
					if client.focus then
						client.focus:toggle_tag(t)
					end
				end,
			}),
			awful.button({
				modifiers = {},
				button = 4,
				on_press = function(t)
					awful.tag.viewprev(t.screen)
				end,
			}),
			awful.button({
				modifiers = {},
				button = 5,
				on_press = function(t)
					awful.tag.viewnext(t.screen)
				end,
			}),
		},
	})
end

-- function _M.create_tasklist(s)
-- 	return awful.widget.tasklist({
-- 		screen = s,
-- 		filter = awful.widget.tasklist.filter.currenttags,
-- 		style = {
-- 			disable_task_name = true,
-- 		},
-- 		layout = {
-- 			spacing_widget = {
-- 				{
-- 					forced_width = 5,
-- 					forced_height = 24,
-- 					thickness = 1,
-- 					color = "none",
-- 					widget = wibox.widget.separator,
-- 				},
-- 				valign = "center",
-- 				halign = "center",
-- 				widget = wibox.container.place,
-- 			},
-- 			spacing = 1,
-- 			layout = wibox.layout.fixed.horizontal,
-- 		},
-- 		widget_template = {
-- 			nil,
-- 			{
-- 				{
-- 					id = "clienticon",
-- 					widget = awful.widget.clienticon,
-- 				},
-- 				margins = {
-- 					top = dpi(2),
-- 					right = dpi(3),
-- 					bottom = dpi(2),
-- 					left = dpi(6),
-- 				},
-- 				widget = wibox.container.margin,
-- 			},
-- 			{
-- 				{
-- 					wibox.widget.base.make_widget(),
-- 					forced_height = dpi(3),
-- 					id = "background_role",
-- 					widget = wibox.container.background,
-- 				},
-- 				margins = dpi(2),
-- 				widget = wibox.container.margin,
-- 			},
-- 			create_callback = function(self, c, index, objects) --luacheck: no unused args
-- 				self:get_children_by_id("clienticon")[1].client = c
-- 			end,
-- 			layout = wibox.layout.align.vertical,
-- 		},
-- 		buttons = {
-- 			awful.button({
-- 				modifiers = {},
-- 				button = 1,
-- 				on_press = function(c)
-- 					c:activate({ context = "tasklist", action = "toggle_minimization" })
-- 				end,
-- 			}),
-- 			awful.button({
-- 				modifiers = {},
-- 				button = 3,
-- 				on_press = function()
-- 					awful.menu.client_list({ theme = { width = 250 } })
-- 				end,
-- 			}),
-- 			awful.button({
-- 				modifiers = {},
-- 				button = 4,
-- 				on_press = function()
-- 					awful.client.focus.byidx(-1)
-- 				end,
-- 			}),
-- 			awful.button({
-- 				modifiers = {},
-- 				button = 5,
-- 				on_press = function()
-- 					awful.client.focus.byidx(1)
-- 				end,
-- 			}),
-- 		},
-- 	})
-- end

local margins = beautiful.system_bar_margins

if vars.status_bar_is_floating then
	if vars.status_bar_position == "bottom" then
		local top = margins.top
		local bottom = margins.bottom

		margins.top = bottom
		margins.bottom = top
	end
else
	margins = {
		top = dpi(0),
		right = dpi(0),
		bottom = dpi(0),
		left = dpi(0),
	}
end

function _M.create_wibox(s)
	return awful.wibar({
		screen = s,
		position = vars.status_bar_position,
		margins = margins,
		height = beautiful.system_bar_height,
		layout = wibox.layout.stack,
		shape = function(cr, width, height)
			gears.shape.rounded_rect(cr, width, height, beautiful.system_bar_corner_radius)
		end,
		widget = {
			-- left widgets
			layout = wibox.layout.fixed.horizontal,
			{
				layout = wibox.layout.fixed.horizontal,
				_M.launcher,
				-- s.taglist,
				-- s.tasklist,
				-- s.promptbox,
			},
			-- middle widgets
			{
				layout = wibox.container.place,
				_M.textclock,
				valign = "center",
				halign = "center",
			},

			-- right widgets
			{
				layout = wibox.layout.fixed.horizontal,
				_M.keyboardlayout,
				focus_mode_widget,
				-- _M.create_system_tray(),
				s.layoutbox,
				margin = dpi(20),
			},
		},
		visible = vars.show_status_bar,
	})
end

return _M
