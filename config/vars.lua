local _M = {}

local is_nil_then_set = require("utilities.functions.is_nil_then_set")

local s = Beautiful.screen

_M.layouts = s.layouts
	or {
		Awful.layout.suit.tile,
		Awful.layout.suit.tile.left,
		Awful.layout.suit.tile.bottom,
		Awful.layout.suit.tile.top,
		Awful.layout.suit.fair,
		Awful.layout.suit.fair.horizontal,
		Awful.layout.suit.spiral,
		Awful.layout.suit.spiral.dwindle,
		Awful.layout.suit.max,
		Awful.layout.suit.max.fullscreen,
		Awful.layout.suit.magnifier,
		Awful.layout.suit.corner.nw,
		Awful.layout.suit.floating,
	}

_M.tags = s.tags or { "1", "2", "3", "4", "5", "6", "7", "8", "9" }
_M.show_tags = is_nil_then_set(s.show_tags, true)

_M.tags_enable_navigation_on_scroll = is_nil_then_set(s.enable_tags_on_navigation_scroll, true)

-- Window configurations
_M.window_enable_titlebar = is_nil_then_set(s.enable_window_titlebar, true)
_M.client_focus_on_window_on_hover = is_nil_then_set(s.focus_client_window_on_hover, true)

-- Status bar configurations
_M.show_status_bar = is_nil_then_set(s.show_status_bar, true)
_M.show_status_bar_on_all_screens = is_nil_then_set(s.show_status_bar_on_all_screens, true)
_M.status_bar_position = s.status_bar_position or "top"
_M.status_bar_show_layout_icon = is_nil_then_set(s.show_layout_icon, true)
_M.status_bar_show_tag_list = is_nil_then_set(s.show_tag_list_on_status_bar, true)
_M.status_bar_show_tasklist = is_nil_then_set(s.show_task_list_on_status_bar, true)
_M.status_bar_show_system_tray = is_nil_then_set(s.show_system_tray, true)
_M.status_bar_is_floating = is_nil_then_set(s.status_bar_is_floating, false)

_M.screen = {
	width = Dpi(Awful.screen.focused().geometry.width),
	height = Dpi(Awful.screen.focused().geometry.height),
}

return _M
