local awful = require("awful")
local beautiful = require("beautiful")

tag.connect_signal("property::layout", function(t)
	if awful.layout.get(mouse.screen) == awful.layout.suit.magnifier then
		awful.tag.setmwfact(beautiful.layout_magnifier_size)
	else
		awful.tag.setmwfact(0.50)
	end
end)
