local awful = require("awful")
local wibox = require("wibox")
local vars = require("config.vars")
local beautiful = require("beautiful")

require("awful.autofocus")

if vars.client_focus_on_window_on_hover == true then
	client.connect_signal("mouse::enter", function(c)
		c:activate({ context = "mouse_enter", raise = false })
	end)
end

client.connect_signal("request::titlebars", function(c)
	-- buttons for the titlebar
	local buttons = {
		awful.button({
			modifiers = {},
			button = 1,
			on_press = function()
				c:activate({ context = "titlebar", action = "mouse_move" })
			end,
		}),
		awful.button({
			modifiers = {},
			button = 3,
			on_press = function()
				c:activate({ context = "titlebar", action = "mouse_resize" })
			end,
		}),
	}

	awful.titlebar(c).widget = {
		-- left
		{
			buttons = buttons,
			layout = wibox.layout.fixed.horizontal,
		},
		-- middle
		{
			-- title
			{
				align = "center",
				widget = awful.titlebar.widget.titlewidget(c),
			},
			buttons = buttons,
			layout = wibox.layout.flex.horizontal,
		},
		-- right
		{
			awful.titlebar.widget.floatingbutton(c),
			awful.titlebar.widget.maximizedbutton(c),
			awful.titlebar.widget.stickybutton(c),
			awful.titlebar.widget.ontopbutton(c),
			awful.titlebar.widget.closebutton(c),
			layout = wibox.layout.fixed.horizontal(),
		},
		layout = wibox.layout.align.horizontal,
	}
end)

client.connect_signal("property::maximized", function(c)
	awful.screen.focused().padding = {
		top = beautiful.useless_gap,
		bottom = beautiful.useless_gap,
		left = beautiful.useless_gap,
		right = beautiful.useless_gap,
	}
end)

client.connect_signal("property::unmaximized", function(c)
	awful.screen.focused().padding = {
		top = 0,
		bottom = 0,
		left = 0,
		right = 0,
	}
end)
