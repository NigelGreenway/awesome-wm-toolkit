local vars = require("config.vars")

local taskbar = require("modules.system")

screen.connect_signal("request::wallpaper", function(s)
	if Beautiful.show_wallpaper == false then
		Gears.wallpaper.set(Beautiful.desktop_background_color)
	else
		Awful.wallpaper({
			screen = s,
			widget = {
				{
					image = Beautiful.screen.wallpaper,
					upscale = true,
					downscale = true,
					widget = Wibox.widget.imagebox,
				},
				valign = "center",
				halign = "center",
				tiled = false,
				widget = Wibox.container.tile,
			},
		})
	end
end)

screen.connect_signal("request::desktop_decoration", function(s)
	Awful.tag(vars.tags, s, Awful.layout.layouts[1])

	if vars.show_status_bar_on_all_screens then
		taskbar(s)
	else
		if s == screen.primary then
			taskbar(s)
		end
	end
end)
