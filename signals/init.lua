return {
	naughty = require("signals.naughty"),
	tag = require("signals.tag"),
	screen = require("signals.screen"),
	client = require("signals.client"),
	ruled = require("signals.ruled"),
	layout = require("signals.layout"),
	-- focus = require("signals.focus"),
}
