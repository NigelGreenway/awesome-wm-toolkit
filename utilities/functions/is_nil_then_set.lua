return function(variable, default_value)
	if variable == nil then
		return default_value
	end

	return variable
end
