local wibox = require("wibox")

local focus_mode_widget = wibox.widget({
	text = (Focus.doNotDisturbMode and "Focus Enabled" or ""),
	widget = wibox.widget.textbox,
})

awesome.connect_signal("focus::toggled", function(text)
	focus_mode_widget:set_markup_silently("<span>" .. text .. "</span>")
end)

return focus_mode_widget
