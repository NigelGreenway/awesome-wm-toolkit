local naughty = require("naughty")
local setmetatable = setmetatable

local rf = assert(io.open(os.getenv("HOME") .. "/.config/awesome.conf", "r"))
local focusMode = rf:read()
rf:close()

local function mysplit(inputstr, sep)
	if sep == nil then
		sep = "%s"
	end
	local t = {}
	for str in string.gmatch(inputstr, "([^" .. sep .. "]+)") do
		table.insert(t, str)
	end
	return t
end

local focus = {
	doNotDisturbMode = mysplit(focusMode, "=")[2] == "true" and true or false,
}

function focus.toggle()
	local f = assert(io.open(os.getenv("HOME") .. "/.config/awesome.conf", "w"))

	if focus.doNotDisturbMode == true then
		focus.doNotDisturbMode = false
	else
		focus.doNotDisturbMode = true
	end

	naughty.toggle()

	f:write("focus_mode=" .. tostring(focus.doNotDisturbMode))
	f:close()

	local content = focus.doNotDisturbMode and "Focus Enabled" or ""

	awesome.emit_signal("focus::toggled", content)
	awesome.emit_signal("added")
end

setmetatable(focus, {
	__call = function(_, ...)
		return focus.toggle()
	end,
})

return focus
