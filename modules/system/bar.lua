local vars = require("config.vars")

local margins = Beautiful.system_bar_margins

local rounded_rect = function(cr, width, height)
	return Gears.shape.rounded_rect(cr, width, height, Beautiful.system_bar_corner_radius)
end

if vars.status_bar_is_floating then
	if vars.status_bar_position == "bottom" then
		local top = margins.top
		local bottom = margins.bottom

		margins.top = bottom
		margins.bottom = top
	end
else
	margins = {
		top = Dpi(0),
		right = Dpi(0),
		bottom = Dpi(0),
		left = Dpi(0),
	}
end

return function(s)
	local bar = Awful.wibar({
		screen = s,
		position = vars.status_bar_position,
		margins = margins,
		height = Beautiful.system_bar_height,
		layout = Wibox.layout.stack,
		shape = Beautiful.system_bar_corner_radius and rounded_rect or nil,
	})

	bar:setup({
		layout = Wibox.layout.stack,
		{
			layout = Wibox.layout.align.horizontal,
			{ -- Left widgets
				layout = Wibox.layout.fixed.horizontal,
			},
			nil,
			{ -- Right widgets
				layout = Wibox.layout.fixed.horizontal,
			},
		},
		{
			Wibox.widget.textclock(),
			valign = "center",
			halign = "center",
			layout = Wibox.container.place,
		},
	})

	return bar
end
