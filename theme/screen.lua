local _screen = {}

_screen.layouts = {
	Awful.layout.suit.tile,
	Awful.layout.suit.magnifier,
	Awful.layout.suit.floating,
}

_screen.tags = { "1", "2", "3", "4" }
_screen.show_tags = false
_screen.enable_tags_on_navigation_scroll = false
_screen.enable_window_titlebar = false
_screen.focus_client_window_on_hover = false
_screen.show_status_bar = true
_screen.show_status_bar_on_all_screens = false
_screen.status_bar_position = "top" -- top, bottom
_screen.show_layout_icon = true
_screen.show_tag_list_on_status_bar = true
_screen.show_task_list_on_status_bar = true
_screen.show_system_tray = true
_screen.status_bar_is_floating = true

_screen.wallpaper = "/home/nigel/.config/awesome/theme/backgrounds/arcane - vi and kaitlyn.jpeg"

return _screen
