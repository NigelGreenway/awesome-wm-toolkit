local awful = require("awful")
local mod = require("bindings.mod")

awful.keyboard.append_global_keybindings({
	-- Volume control
	awful.key({
		modifiers = {},
		key = "XF86AudioRaiseVolume",
		description = "Volume up (by 1%)",
		group = "Media",
		on_press = function()
			awful.util.spawn("amixer -D pulse sset Master 1%+", false)
		end,
	}),
	awful.key({
		modifiers = { mod.ctrl },
		key = "XF86AudioRaiseVolume",
		description = "Volume up (by 5%)",
		group = "Media",
		on_press = function()
			awful.util.spawn("amixer -D pulse sset Master 5%+", false)
		end,
	}),
	awful.key({
		modifiers = {},
		key = "XF86AudioLowerVolume",
		description = "Volume down (by 1%)",
		group = "Media",
		on_press = function()
			awful.util.spawn("amixer -D pulse sset Master 1%-", false)
		end,
	}),
	awful.key({
		modifiers = { mod.ctrl },
		key = "XF86AudioLowerVolume",
		description = "Volume down (by 5%)",
		group = "Media",
		on_press = function()
			awful.util.spawn("amixer -D pulse sset Master 5%-", false)
		end,
	}),
	-- Music controls
	awful.key({
		modifiers = {},
		key = "XF86AudioPlay",
		description = "Play/Pause media",
		group = "Media",
		on_press = function()
			awful.util.spawn(
				"dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.PlayPause"
			)
		end,
	}),
	awful.key({
		modifiers = {},
		key = "XF86AudioPrev",
		description = "Previous track",
		group = "Media",
		on_press = function()
			awful.util.spawn(
				"dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Previous"
			)
		end,
	}),
	awful.key({
		modifiers = {},
		key = "XF86AudioNext",
		description = "Next track",
		group = "Media",
		on_press = function()
			awful.util.spawn(
				"dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Next"
			)
		end,
	}),
})
